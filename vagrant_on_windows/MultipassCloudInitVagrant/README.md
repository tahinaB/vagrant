

# Activation d'hyper-v, SMB/CIFS

```pwsh
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
Enable-WindowsOptionalFeature -Online -FeatureName SMB1Protocol
```

# With Multipass, cloud init, vagrant and Ansible

- Download last [release](https://github.com/canonical/multipass/releases)
- Check if multipass have create VM at install 

```pwsh
multipass list
```

and delete it if exists : 

```pwsh
multipass delete [vm name]
multipass purge
```

- Make *dev* as VM default name
```pwsh
multipass set client.primary-name=ubuntuFromMultipass
```

- Create a public/private ssh key with ```ssh-keygen -t ed25519``` and store it in key\.ssh directory

- Create cloud-init.yaml file for **Cloud unit**
```pwsh
New-Item -Path . -ItemType "file" -Name "cloud-init.yaml"
```

- Provisioning a VM Ubuntu Mantic with 4 cpu and 6 Go of memory and 20G of Disk  : 
```pwsh 
multipass launch mantic -n ubuntuFromMultipass -c 4 -m 6G -d 20G --cloud-init  cloud-init.yaml
```

- Enable embedded virtualization on VM dev
```pwsh
multipass stop ubuntuFromMultipass
Set-VMProcessor -VMName ubuntuFromMultipass -ExposeVirtualizationExtensions $True
multipass start ubuntuFromMultipass
multipass shell ubuntuFromMultipass
```

