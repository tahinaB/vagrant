
# Activation d'hyper-v, SMB/CIFS

```pwsh
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
Enable-WindowsOptionalFeature -Online -FeatureName SMB1Protocol
```

# Vagrant
- Download last [release](https://developer.hashicorp.com/vagrant/install)

```pwsh
vagrant box add [box name]
vagrant init [box name]
```

# Ansible playbook
- Get the playbook from ```https://gitlab.com/dev-ops17/ubuntu_ansible_playbook.git``` and put it in /playbooks directory 