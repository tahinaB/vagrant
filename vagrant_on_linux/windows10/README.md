# Install freerdp to connect to Windows using RDP protocol:

```sh
apt-get install freerdp2-x11
vagrant rdp -- /cert-ignore
```

https://app.vagrantup.com/peru/boxes/windows-10-enterprise-x64-eval